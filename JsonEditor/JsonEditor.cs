﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Fiddler;
using System.Windows.Forms.Integration;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json.Linq;

[assembly: Fiddler.RequiredVersion("4.4.2.1")]

namespace JsonEditor
{
    #region "       JsonEditor      "

    public class JsonEditor : Inspector2
    {
        public JsonEditor()
        {
            Editor = new JsonEditorControl();
            Editor.InitializeComponent();
        }

        #region "       Properties      "

        protected JsonEditorControl Editor { get; set; }

        #endregion

        #region "       Methods     "

        public override void AddToTab(System.Windows.Forms.TabPage tabPage)
        {
            ElementHost elementHost = null;

            elementHost = new ElementHost();
            elementHost.Dock = DockStyle.Fill;
            elementHost.Child = Editor;

            tabPage.Controls.Add(elementHost);
            tabPage.Text = "Json Editor";
        }

        public override int GetOrder()
        {
            return 0;
        }

        public override bool UnsetDirtyFlag()
        {
            this.Editor.UnsetDirtyFlag();

            return base.UnsetDirtyFlag();
        }

        public override int ScoreForContentType(string sMIMEType)
        {
            return sMIMEType.ToLower().Contains("json") ? 100 : 0;
        }

        #endregion
    }

    #endregion

    #region "       JsonResponseEditor      "

    public class JsonResponseEditor : JsonEditor, IResponseInspector2
    {
        public HTTPResponseHeaders ResponseHeaders { get; set; }

        #region "       IResponseInspector2     "

        HTTPResponseHeaders IResponseInspector2.headers
        {
            get
            {
                return ResponseHeaders;
            }
            set
            {
                ResponseHeaders = value;
            }
        }

        void IBaseInspector2.Clear()
        {
            this.Editor.Clear();
        }

        bool IBaseInspector2.bDirty
        {
            get { return this.Editor.IsDirty; }
        }

        bool IBaseInspector2.bReadOnly
        {
            get
            {
                return this.Editor.IsReadOnly;
            }
            set
            {
                this.Editor.IsReadOnly = value;
            }
        }

        byte[] IBaseInspector2.body
        {
            get { return this.Editor.Body; }
            set { this.Editor.Body = value; }
        }

        #endregion
    }

    #endregion

    #region "       JsonRequestEditor      "

    public class JsonRequestEditor : JsonEditor, IRequestInspector2
    {
        public HTTPRequestHeaders RequestHeaders { get; set; }

        #region "       IRequestInspector2     "

        HTTPRequestHeaders IRequestInspector2.headers
        {
            get
            {
                return RequestHeaders;
            }
            set
            {
                RequestHeaders = value;
            }
        }

        void IBaseInspector2.Clear()
        {
            this.Editor.Clear();
        }

        bool IBaseInspector2.bDirty
        {
            get { return this.Editor.IsDirty; }
        }

        bool IBaseInspector2.bReadOnly
        {
            get
            {
                return this.Editor.IsReadOnly;
            }
            set
            {
                this.Editor.IsReadOnly = value;
            }
        }

        byte[] IBaseInspector2.body
        {
            get { return this.Editor.Body; }
            set { this.Editor.Body = value; }
        }

        #endregion
    }

    #endregion
}
