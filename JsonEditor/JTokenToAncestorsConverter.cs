﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Newtonsoft.Json.Linq;

namespace JsonEditor
{
    public class JTokenToAncestorsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            JToken jtoken = value as JToken;

            if (jtoken == null)
                return null;
            else
                return jtoken.GetJTokenAncestors();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
