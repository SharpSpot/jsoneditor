﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using Newtonsoft.Json.Linq;

namespace JsonEditor
{
    public class ItemTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            DataTemplate dataTemplate = null;

            if (item == null || element == null)
                return null;
            else
            {
                if (item is JProperty)
                {
                    if (((JProperty)item).Value is JArray)
                        return element.FindResource("JPropertyWithArrayTemplate") as DataTemplate;
                    else if (((JProperty)item).Value is JObject)
                        return element.FindResource("JPropertyWithObjectTemplate") as DataTemplate;
                    else if (((JProperty)item).Value is JValue)
                        return element.FindResource("JPropertyWithValueTemplate") as DataTemplate;
                }
                else if (item is JObject)
                    return element.FindResource("JObjectTemplate") as DataTemplate;
				else if (item is JArray)
					return element.FindResource("JArrayTemplate") as DataTemplate;
                else if (item is JValue)
                    return element.FindResource("JValueTemplate") as DataTemplate;
            }

            if (dataTemplate == null)
                dataTemplate = element.FindResource("ErrorTemplate") as DataTemplate;

            return dataTemplate;
        }
    }
}
