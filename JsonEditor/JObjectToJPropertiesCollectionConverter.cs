﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Newtonsoft.Json.Linq;
using System.Windows.Controls;

namespace JsonEditor
{
    public class JObjectToJPropertiesCollectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IEnumerable<JToken> tokenCol = null;

            if (parameter != null)
            {
                tokenCol = this.GetPropertyColFromJObject(value, parameter);

                if (tokenCol == null)
                    tokenCol = this.GetPropertyColFromJArray(value, parameter);
            }

            return tokenCol;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<JProperty> GetPropertyColFromJObject(object value, object parameter)
        {
            JProperty jProperty = null;
            JObject jObject = null;
            IEnumerable<JProperty> propertyCol = null;

            jObject = value as JObject;

            if (jObject == null)
            {
                jProperty = value as JProperty;

                if (jProperty != null)
                    jObject = jProperty.Value as JObject;
                //Check for root node...
                else if (value is TreeViewItem)
                    jObject = ((TreeViewItem)value).Header as JObject;
            }

            if (jObject != null)
            {
                switch (parameter.ToString().ToUpper())
                {
                    case "VALUES":
                        propertyCol = jObject.Properties().Where(t => t.Value is JValue).OrderBy(p => p.Name);
                        break;
                    case "OBJECTS":
                        propertyCol = jObject.Properties().Where(t => t.Value is JObject).OrderBy(p => p.Name);
                        break;
                    case "ARRAYS":
                        propertyCol = jObject.Properties().Where(t => t.Value is JArray).OrderBy(p => p.Name);
                        break;
                }
            }

            return propertyCol;
        }

        private IEnumerable<JToken> GetPropertyColFromJArray(object value, object parameter)
        {
            JProperty jProperty = null;
            JArray jArray = null;
            IEnumerable<JToken> propertyCol = null;

            jArray = value as JArray;

            if (jArray == null)
            {
                jProperty = value as JProperty;

                if (jProperty != null)
                    jArray = jProperty.Value as JArray;
                //Check for root node...
                else if (value is TreeViewItem)
                    jArray = ((TreeViewItem)value).Header as JArray;
            }

            if (jArray != null)
            {
                switch (parameter.ToString().ToUpper())
                {
                    case "VALUES":
                        propertyCol = jArray.OfType<JValue>().OrderBy(p => p.Value);
                        break;
                    case "OBJECTS":
                        propertyCol = jArray.OfType<JObject>();
                        break;
                    case "ARRAYS":
                        propertyCol = jArray.OfType<JArray>();
                        break;
                }
            }

            return propertyCol;
        }
    }
}
