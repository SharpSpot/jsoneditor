﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Newtonsoft.Json.Linq;

namespace JsonEditor
{
    public class JTokenToIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            JToken jToken = value as JToken;

            if (jToken != null && jToken.Parent != null && jToken.Parent.Type == JTokenType.Array)
                return ((JArray)jToken.Parent).IndexOf(jToken);
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
