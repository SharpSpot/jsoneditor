### Project Description ###
Json Editor is a Fiddler v4 inspector that reads and writes json for the selected session. 
The project utilizes an ElementHost control to host WPF.  The JSON from each session is parsed using Json.NET.  This code uses a specific version of Json.NET (v4.0) in order to work in WPF.

### NOTE:  This inspector requires Fiddler v4. ###

### Features: ###

* TreeView navigation
* Bread crumbs
* Group Views on selected item (properties, objects, arrays)
* Quick navigation from bread crumb or group viewers
* Edit, Copy, Paste, and Remove functions
* Create new items
* Paste raw JSON from clipboard
* Find All text search


![screenshot.png](https://bitbucket.org/repo/8zjpbnK/images/2998459597-screenshot.png)
 
### Installation: ###

Download the binary files and copy them into the inspector folder of your Fiddler install.  Typically this is C:\Program Files (x86)\Fiddler2\Inspectors.